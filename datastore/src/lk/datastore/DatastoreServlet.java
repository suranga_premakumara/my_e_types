package lk.datastore;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class DatastoreServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		Entity e = new Entity("person","student");
		
		e.setProperty("first_name", "suranga");
		e.setProperty("lastname", "Premakumara");		
	
		
		ds.put(e);
		
		
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, Welcome to GAE DataStore aoo salli ");
		
		// creating entity with identifire
		
		Entity user = new Entity("user",100);
		user.setProperty("user_id", 100);
		user.setProperty("user_name", "suranga");
		user.setProperty("User_age", "24");
		user.setProperty("User_Adress", "ITfac,uom");
		
		ds.put(user);
		
		//batch entity
		
		Entity engine = new Entity("Audi");
		
		engine.setProperty("engine_type", "1000CC");
		engine.setProperty("engine_Price", "4000CC");
		
		Entity engine2 = new Entity("VoxWagon");
		Entity engine3 = new Entity("Toyota");

		List<Entity> en =Arrays.asList(engine,engine2,engine3);
		
		ds.put(en);
		
	
      //  Key todoKey = KeyFactory.createKey("user", "user_name");
    //    Entity entity;
		try {
		 Key key = KeyFactory.createKey("person","student");
	        Entity userentity = ds.get(key);
        
        	userentity = ds.get(key);
            String fname = (String) userentity.getProperty("first_name");
            String lname = (String) userentity.getProperty("lastname");
            resp.setContentType("text/plain");
            resp.getWriter().println("Got the details with the summary name " + fname+" "+lname);
        } catch (EntityNotFoundException enf) {
            resp.setContentType("text/plain");
            resp.getWriter().println("Entry not found.");
        }

		
	}
}
